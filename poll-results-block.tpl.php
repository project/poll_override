<div class="poll">
  <div class="title"><?php print $title ?></div>
  <?php print $results ?>
  <div class="total">
    <?php print t('Total votes: @votes', array('@votes' => $votes)); ?>
  </div>
  <?php
  if ( isset($message) ) {
    print '<p class="voted">'.$message.'</p>';
  }
  ?> 
</div>
<div class="links"><?php print $links; ?></div>
